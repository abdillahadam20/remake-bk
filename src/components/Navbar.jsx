// import React from 'react'
// import { Logo } from '../assets/logo.png'
import LogoImages from "../assets/logo.png";



const Navbar = () => {
  const menus = [
    {
      name: 'delivery',
      label: 'Order'
    },
    {
      name: 'Get Fresh',
      label: 'Promotions'
    },
    {
      name: 'Exclusive',
      label: 'Large Order'
    }
  ]

  return (
    <div className="bg-[#2d2d2d] shadow-md">
      <div className="max-w-7xl mx-auto">
        <div className="flex ">
          {/* Left navbar */}
          <div className="flex items-center gap-8">
          <div className="bg-white rounded-full w-24 h-24 flex justify-center items-center abs shadow-md relative top-4">
                <img src={LogoImages} alt="" className="w-16 h-16" />
              </div>
            <ul className="flex gap-4">
              {menus.map(menu => {
                // console.log(menu)
                <li key={menu.name}>
                  <a href="#">{menu.name}</a>
                </li>
              })}
              {/* <li>
                <a href="#" className="text-[#faaf18] flex flex-col text-xs">
                  Delivery
                  <span className="text-2xl text-white font-bold">Order</span>
                </a>
              </li>
              <li>
                <a href="#" className="text-[#faaf18] flex flex-col text-xs">
                  Delivery
                  <span className="text-2xl text-white font-bold">Order</span>
                </a>
              </li>
              <li>
                <a href="#" className="text-[#faaf18] flex flex-col text-xs">
                  Delivery
                  <span className="text-2xl text-white font-bold">Order</span>
                </a>
              </li> */}
            </ul>
          </div>
          {/* Right navbar */}
          <div className=""></div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
